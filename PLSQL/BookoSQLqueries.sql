select * from pm_discount_definition@prd  a, pm_discount_category@prd b 
where a.discount_def_seq = b.discount_def_seq 
  and a.coupon_code = 'CPCUMBNFT';
  
select * from pm_discount_category@prd;
select * from pm_discount_definition@prd;

select * from op_status_codes;

-- View all products in Production
select * from exam@prd;
select * from exam@prd where ex_abbrev like '%SPPA%';

select * from inventory_header@prd;
select * from inventory_header@prd where class = 'SV';
select * from inventory_header@prd where program = '46' and elec_orders = 'N' or program = '47' and elec_orders = 'N';
select * from inventory_header@prd where item_seq = '23728';
select * from inventory_header@prd where description like '%International Insurance Fundamentals%';

select * from exam_program@prd;
select * from exam_program@prd where program = '45';
select * from inventory_book@prd;
select * from inventory_book@prd where exam = '930';

select * from pm_product_list_v@prd;
select * from pm_product_list_v@prd where item_seq = '23162';
select * from pm_product_list_v@tst;
select * from pm_product_list_v@prd where description like '%International Insurance Fundamentals%';

select * from inventory_package@prd where product like '%SPPA%';

-- Determine category_seq #
select * from pm_product_category@dev where item_seq = '19698';
select * from pm_product_category@tst where item_seq = '31989';
select * from pm_product_category@prd where category_seq = '221';
select * from pm_product_category@prd where item_seq = '31989';

-- Find category_seq #
select * from pm_category@dev;
select * from pm_category@tst where label like '%IIF%';
select * from pm_category@prd where label like '%IIF%';

-- Accounts
-- find the billable account's contact_seq
select * from contact@prd, billable_account@prd where contact.id = 'A63880001' and contact.contact_seq = billable_account.contact_seq;
select * from contact_credential@prd where contact_seq = '1858849';

BEGIN
for i in (select item_seq from inventory_header@prd where alias like '%AIC44%')
LOOP
insert into inventory_header@tst
select * from inventory_header@prd where item_seq = i.item_seq;
insert into inventory_price@tst
select * from inventory_price@prd where item_seq = i.item_seq;
insert into inventory_line@tst
select * from inventory_line@prd where item_seq = i.item_seq;
insert into inventory_package@tst
select * from inventory_package@prd where item_seq = i.item_seq;
insert into inventory_book@tst
select * from inventory_book@prd where item_seq = i.item_seq;
insert into virtual_class@tst
select * from virtual_class@prd where item_seq = i.item_seq;
insert into courses@tst
select * from courses@prd
where course_seq in (
select course_seq from virtual_class@prd  where
item_seq in (i.item_seq) );

END LOOP;
COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;
END;
