/* Query Records that are missing a SECURITY_GROUP */
SELECT N.ID, N.LAST_FIRST, N.MEMBER_TYPE, NSG.SECURITY_GROUP, N.EMAIL, NS.WEB_LOGIN, N.UPDATED_BY, NS.UPDATED_BY, N.DATE_ADDED, N.PAID_THRU
      FROM CPCU1.dbo.Name N
      INNER JOIN CPCU1.dbo.Name_Security NS ON N.ID = NS.ID
      INNER JOIN CPCU1.dbo.Name_Security_Groups NSG ON N.ID = NSG.ID
WHERE
      N.MEMBER_TYPE IN ('M','CAN','ND')
      AND NS.WEB_LOGIN = ''
      AND PAID_THRU != ''
      OR
      N.MEMBER_TYPE IN ('M','CAN','ND')
      AND SECURITY_GROUP = ''
      AND PAID_THRU != ''
ORDER BY
      N.LAST_FIRST ASC;

/* Update Records that are missing a SECURITY_GROUP */
UPDATE
      NSG
SET
      NSG.SECURITY_GROUP = 'Members'
FROM
      CPCU1.dbo.Name N
      INNER JOIN CPCU1.dbo.Name_Security NS ON N.ID = NS.ID
      INNER JOIN CPCU1.dbo.Name_Security_Groups NSG ON N.ID = NSG.ID
WHERE
      N.MEMBER_TYPE IN ('M','CAN','ND')
      AND NS.WEB_LOGIN = ''
      AND PAID_THRU != ''
      OR
      N.MEMBER_TYPE IN ('M','CAN','ND')
      AND SECURITY_GROUP = ''
      AND PAID_THRU != '';

/* Update individual records that are missing a SECURITY_GROUP */
UPDATE
      CPCU1.dbo.Name_Security_Groups
SET
      SECURITY_GROUP = 'Members'
WHERE
      ID = '2368916';